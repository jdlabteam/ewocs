﻿* Encoding: UTF-8.

*Basic Social Approach Analyses for sociability trial investigation time. The 'Social Approach Test Dataset.sav' is also provided as an example dataset.
*For analysis of your own dataset, adjust variable names as needed. 
 

*Set file path. 
cd 'INSERT FILE PATH'.
*Pull in dataset. Here is an example for an excel file.  
GET DATA
  /TYPE=XLSX
  /FILE='Social Approach Test Dataset.xlsx'
  /SHEET=name 'DATA'
  /CELLRANGE=FULL
  /READNAMES=ON
  /DATATYPEMIN PERCENTAGE=95.0
  /HIDDEN IGNORE=YES.
EXECUTE.
DATASET NAME short.


*Dummy code variables.
AUTORECODE VARIABLES=Sex$ Group$
  /INTO Sex Group
  /PRINT.

VARIABLE ALIGNMENT ALL (center).
VARIABLE WIDTH ALL (10).

*Save dataset file and output file.
SAVE OUTFILE='Social Approach Test Data Short.sav'.

Output SAVE OUTFILE='Social Approach Test.spv'.

*Option to set a previously defined chart template. 
SET CTemplate='Social Approach Chart Template.sgt'.

*Calculate the Social Preference Index.
Compute Social_Pref_Index=(Social_Invest_Time/ (Social_Invest_Time + Empty_Invest_Time))*100.
Execute.
VARIABLE ALIGNMENT ALL (center).
VARIABLE WIDTH ALL (10).


*Check Normality, Outliers, Variance; First save z scores for normality.
SORT CASES  BY Group.
SPLIT FILE LAYERED BY Group.
DESCRIPTIVES VARIABLES=Social_Invest_Time Empty_Invest_Time Social_Pref_Index
  /SAVE
  /STATISTICS=MEAN VARIANCE SEMEAN KURTOSIS SKEWNESS.
Split file off.
VARIABLE WIDTH ALL (10). 
VARIABLE ALIGNMENT ALL (center). 
SAVE OUTFILE='Social Approach Test Data Short.sav'.

*Next, examine z scores for normality.
EXAMINE VARIABLES=ZSocial_Invest_Time ZEmpty_Invest_Time ZSocial_Pref_Index BY Group
 /PLOT BOXPLOT HISTOGRAM NPPLOT
  /STATISTICS NONE
  /CINTERVAL 95
  /MISSING PAIRWISE
  /NOTOTAL.
*Use pairwise for missing so all variables are not affected when one variable has missing values.

*Z scores descriptives.
SORT CASES  BY Group.
SPLIT FILE LAYERED BY Group.
DESCRIPTIVES VARIABLES=ZSocial_Invest_Time ZEmpty_Invest_Time ZSocial_Pref_Index
 /STATISTICS= KURTOSIS SKEWNESS.
Split file off.


*If variance across levels of the repeated measure (social and empty stimuli) is homogeneous, you may consider pooling the error (recommended)
 for simple main effects of Group within each stimulus. 
*To do so, need to create a longform version of the data. 
*Create a longform. 
SAVE OUTFILE='Social Approach Test Data Short.sav'.
VARSTOCASES
    /MAKE Time FROM Social_Invest_Time Empty_Invest_Time
   /INDEX=SIDE$(Time) 
  /KEEP=ID
Group
Sex
  /NULL=KEEP.
AUTORECODE VARIABLES=SIDE$
  /INTO Side
  /PRINT.
Value labels Side
1 'Empty'
2 'Social'.
VARIABLE ALIGNMENT ALL (center).
VARIABLE WIDTH ALL (10).
SAVE OUTFILE='Social Approach Test Data Longform.sav'.
DATASET NAME longform.

GET 
  FILE='Social Approach Test Data Short.sav'. 
DATASET NAME short.


*Repeated Measures (rm)ANOVA.
DATASET ACTIVATE short.
GLM Social_Invest_Time Empty_Invest_Time BY Group
  /WSFACTOR=Side 2 Polynomial 
  /METHOD=SSTYPE(3)
  /PLOT=PROFILE(Side*Group) TYPE=BAR ERRORBAR=SE(1) MEANREFERENCE=NO
  /PRINT=DESCRIPTIVE ETASQ OPOWER HOMOGENEITY 
  /CRITERIA=ALPHA(.05)
  /WSDESIGN=Side 
  /DESIGN=Group.

*If significant Group x Side interaction, re-run the rmANOVA with simple main effects.
*If variance across levels of the repeated measure (Stimulus) are heterogenous, do not pool the error and run the syntax below.
DATASET ACTIVATE short.
GLM Social_Invest_Time Empty_Invest_Time BY Group
  /WSFACTOR=Side 2 Polynomial 
  /METHOD=SSTYPE(3)
  /PLOT=PROFILE(Side*Group) TYPE=BAR ERRORBAR=SE(1) MEANREFERENCE=NO
    /EMMEANS=TABLES(Side*Group) compare(Side) Adj(BONFERRONI)
    /EMMEANS=TABLES(Side*Group) compare(Group) Adj(BONFERRONI)
  /PRINT=DESCRIPTIVE ETASQ OPOWER HOMOGENEITY 
  /CRITERIA=ALPHA(.05)
  /WSDESIGN=Side 
  /DESIGN=Group.


*If variance across levels of the repeated measure (Stimulus) are homoegenous, use the pooled error and increased decreased of freedom
that are derived from the syntax line 143 [ /EMMEANS=TABLES(Side*Group) compare(Group)]. Ignore the rest of the output from the UNIANOVA.
DATASET ACTIVATE short.
GLM Social_Invest_Time Empty_Invest_Time BY Group
  /WSFACTOR=Side 2 Polynomial 
  /METHOD=SSTYPE(3)
  /PLOT=PROFILE(Side*Group) TYPE=BAR ERRORBAR=SE(1) MEANREFERENCE=NO
    /EMMEANS=TABLES(Side*Group) compare(Side) Adj(BONFERRONI)
  /PRINT=DESCRIPTIVE ETASQ OPOWER HOMOGENEITY 
  /CRITERIA=ALPHA(.05)
  /WSDESIGN=Side 
  /DESIGN=Group.
DATASET ACTIVATE longform.
UNIANOVA Time BY Side Group
  /METHOD=SSTYPE(3)
  /INTERCEPT=INCLUDE
  /EMMEANS=TABLES(Side*Group) compare(Group) Adj(BONFERRONI)
  /CRITERIA=ALPHA(0.05)
  /DESIGN=Side Group Side*Group.



*If the rmANOVA did not produce a significant Side x Group interaction, run a paired t-test on investigation time for the Control Group only
to determine if the controls demonstrate an expected preference.

*Select the control group only.
DATASET ACTIVATE short.
USE ALL.
COMPUTE filter_$=(Group = 1).
VARIABLE LABELS filter_$ 'Group = 1 (FILTER)'.
VALUE LABELS filter_$ 0 'Not Selected' 1 'Selected'.
FORMATS filter_$ (f1.0).
FILTER BY filter_$.
EXECUTE.

*Paired t-test.
T-TEST PAIRS=Social_Invest_Time WITH Empty_Invest_Time (PAIRED)
  /CRITERIA=CI(.9500)
  /MISSING=ANALYSIS.
Use All.

*If Control group spent signicantly more time with the Social Stimulus compared to the Empty Stimulus, run an independent t-test on the 
Social Preference Index between Groups.

*Independent t-test.
T-TEST GROUPS=Group(1 2)
  /MISSING=ANALYSIS
  /VARIABLES=Social_Pref_Index
  /CRITERIA=CI(.95).




*Export Means and standard error. 
dataset activate short.
*OPEN NEW OUTPUT FOR THIS.
sort cases by Group.
split file layered by Group.
DESCRIPTIVES VARIABLES=Social_Invest_Time Empty_Invest_Time Social_Pref_Index
  /STATISTICS=MEAN SEMEAN.
SPLIT FILE OFF.
Export Output.
OUTPUT EXPORT
  /CONTENTS  EXPORT=VISIBLE  LAYERS=PRINTSETTING  MODELVIEWS=PRINTSETTING
  /XLSX  DOCUMENTFILE='Social Approach Test MEANS AND SEM.xlsx'
     OPERATION=CREATEFILE
     LOCATION=LASTCOLUMN  NOTESCAPTIONS=YES.


*Save each file as an excel file.
DATASET ACTIVATE short. 
SAVE TRANSLATE OUTFILE='Social Approach Test Short SPSS.xlsx'
  /TYPE=XLS
  /VERSION=12
  /MAP
  /FIELDNAMES VALUE=NAMES
  /CELLS=VALUES.

DATASET ACTIVATE longform. 
SAVE TRANSLATE OUTFILE='Social Approach Test Longform SPSS.xlsx'
   /TYPE=XLS
   /VERSION=12
  /MAP
  /FIELDNAMES VALUE=NAMES
  /CELLS=VALUES.








